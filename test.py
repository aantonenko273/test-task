#-*- coding: utf-8 -*-
from pprint import pprint
import csv
import json
import sys


def testResults(fileName):
    """This function tests result file"""
    itemsTotal = 0
    testResultDic = {'skuFilled': 0,
                     'nameFilled': 0,
                     'itemCategoryFilled': 0,
                     'sizeFilled': 0,
                     'descriptionFilled': 0,
                     'currencyFilled': 0,
                     'priceFilled': 0,
                     'inStockFilled': 0,
                     'colourFilled': 0,
                     'incorrectCurrency': 0}

    with open(fileName) as inputFile:
        reader = csv.reader(inputFile, delimiter='|', )
        next(inputFile)
        for row in reader:
            itemsTotal += 1

            if row[7]:
                if row[0] == 'fr' and row[7] != 'eur': testResultDic['incorrectCurrency'] += 1
                elif row[0] == 'us' and row[7] != 'usd': testResultDic['incorrectCurrency'] += 1

            if row[2] != '': testResultDic['skuFilled'] += 1
            if row[3] != '': testResultDic['nameFilled'] += 1
            if row[4] != '': testResultDic['itemCategoryFilled'] += 1
            if row[5] != '': testResultDic['sizeFilled'] += 1
            if row[6] != '': testResultDic['descriptionFilled'] += 1
            if row[7] != '': testResultDic['currencyFilled'] += 1
            if row[8] != '': testResultDic['priceFilled'] += 1
            if row[9] != 'False': testResultDic['inStockFilled'] += 1
            if row[10] != '': testResultDic['colourFilled'] += 1


    for key,value in testResultDic.items():
        testResultDic[key] = round(value * 100 / itemsTotal, 2)

    testResultDic['itemsTotal'] = itemsTotal
    pprint(testResultDic)

    with open('./'+fileName.split('.')[0]+'.json', 'w') as resultFile:
        resultFile.write(json.dumps(testResultDic))


fileName = sys.argv[-1]
if fileName.split('.')[-1] == 'csv':
    testResults(fileName)
else:
    print('Incorrect usage. \n Usage example: python3.7 test.py fileName.csv')
