Crawlers are based on scrapy, requests & BeautifulSoup libraries. For general logics please see comments in python files. 

# .py files explanation

*frDior.py* - crawls french version of dior.com

*usDior.py* - crawls us version of dior.com

*test.py* - looks into passed filename.csv, tests it and writes result to filename.json. Values are in %.

# ~/results/ files explanation

results/*.csv – parsed items from us/fr versions of dior.com

results/*.log – scrapy logs

results/*.json – test.py results






