#-*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy import Request
from pprint import pprint
from bs4 import BeautifulSoup, Comment
import scrapy
import requests
import ast
import time
import csv

class UsDiorSpider(scrapy.spiders.CrawlSpider):
    """Designed to crawl US version of dior.com"""

    name = 'UsDior'
    allowed_domains = ['www.dior.com']
    denyList = ['/fr_fr/', '/ko_kr/', '/ja_jp/', '/en_hk/', '/zh_hk/', '/zh_cn/', '/en_sam/', '/pt_br/', '/en_gb/',
                '/ru_ru/','/nl_nl/', '/it_it/','/zh_tw/', '/es_es/', '/de_de/', '/nl_be/', '/fr_be/', 'q=', '-show',
                'the-books', 'article', 'awarded', 'videos']

    start_urls = ['https://www.dior.com/en_us/']
    rules = ( Rule(LinkExtractor(allow=('/en_us/', ) , deny=(denyList) ) , callback='processLink', follow=True) , )

    processedProducts = []
    fileName = 'dior_us.csv'
    file = open(fileName, 'w')
    file.write('region|webCategory|SKU|name|itemCategory|size|description|currency|price|inStock|colour|processTime\n')
    file.close()

    def isProduct(self, link):
        """This function checks if link is product, also returns item's productId"""

        isProduct = False
        productId = None

        if link.split('-')[-1].isdigit():
            isProduct = True

            if link.split('-')[-2].isdigit():
                productId = ''.join(link.split('-')[-2:])

            if not productId: productId = link.split('-')[-1]

        elif len(link.split('-y')) > 1:
            if link.split('-y')[1].split('-')[0].isdigit():
                isProduct = True
                productId = link.split('-y')[1].split('-')[0]

            elif link.split('-y')[1].split('_')[0].isdigit():
                isProduct = True
                productId = ''.join([x for x in link.split('-y')[1].split('-')[0] if x.isdigit()])

        return isProduct, productId

    def processLink(self, response):
        """By calling isProduct() this function checks if current link is valid item, calls processItem() and saves result to file"""
        startTime = time.time()
        link = response.url
        isProduct, productId = self.isProduct(link)

        if isProduct and len(link.split('/')[-1].split('-')) > 3:

            if productId not in self.processedProducts:

                print('\nProcessing..' + link)

                self.processedProducts.append(productId)
                itemDic = self.processItem(response, startTime)

                with open(self.fileName, 'a') as file:
                    writer = csv.writer(file, delimiter='|')
                    writer.writerow([x[1] for x in itemDic.items()])

            else:
                print('\nDuplicate!!', link)

    def processItem(self, response, startTime):
        """This function process response object using BeautifulSoup and appends filled itemDic to result .csv file"""
        itemDic = {'region':'',
                   'webCategory':'',
                   'SKU':'',
                   'name':'',
                   'itemCategory':'',
                   'size':'',
                   'description':'',
                   'currency':'',
                   'price':'',
                   'inStock': False,
                   'colour':'',
                   'processTime':''}

        if 'fr_fr' in response.url:
            itemDic['region'] = 'fr'

        elif 'en_us' in response.url:
            itemDic['region'] = 'us'

        itemDic['webCategory'] = '/'.join(response.url.split('www.dior.com/')[1:][0].split('/')[:-1])

        soup = BeautifulSoup(response.text, 'lxml')

        if soup.find('meta', {'name':'gsa-sku'}):
            if '|' in soup.find('meta', {'name':'gsa-sku'}).get('content'):
                itemDic['SKU'] = soup.find('meta', {'name':'gsa-sku'}).get('content').split('|')[0]
            else:
                itemDic['SKU'] = soup.find('meta', {'name':'gsa-sku'}).get('content')

        if soup.find('div', {'class':'c-breadcrumb max-1440'}):
            breadCrumb = [x.text for x in
                          soup.find('div',{'class':'c-breadcrumb max-1440'}).find_all('li')][1:]

        elif soup.find('ul', {'class':'breadcrumb'}):
            breadCrumb = [x[0] for x in [x.text.split('\n                        ') for x in
                          soup.find('ul',{'class':'breadcrumb'}).find_all('li') if '>' not in x]]

        else: breadCrumb = None

        if breadCrumb:
            itemDic['name'] = breadCrumb[-1]
            itemDic['itemCategory'] = '/'.join(breadCrumb[:-1])


        # Figure out size
        # If diameter in item name, its most likely a watch
        if 'ø' in itemDic['name'].lower():
            itemDic['size'] = itemDic['name'].lower().split('ø ')[1].split(', ')[0]

        elif soup.find(
                'button', {'aria-controls':'product-description-section-1'}):

            if soup.find(
                    'button', {'aria-controls':'product-description-section-1'}).findNext('div'):

                specs = soup.find(
                            'button', {'aria-controls':'product-description-section-1'}).findNext('div').text
                size = [size for size in specs.split('\n') if ' cm' in size or ' mm' in size]
                if size:
                    itemDic['size'] = '. '.join(size)


        # Figure out item description
        if soup.find('div', {'class':'product-description-content'}):
            itemDic['description'] = soup.find(
                                        'div', {'class':'product-description-content'}).text

        elif soup.find('div', {'class':'block-fp block-fp--text'}):
            itemDic['description'] = soup.find(
                                        'div', {'class':'block-fp block-fp--text'}).findNext('p').text

        # If no size yet, perhaps its hidden in item description
        if not itemDic['size']:

            if ' cm' or ' mm' in itemDic['description'].split('/')[-1]:
                size = [size for size in itemDic['description'].split('\n') if ' cm' in size or ' mm' in size]

                if size:
                    itemDic['size'] = '. '.join(size)

        # If price/currency is present on page
        if soup.find('span',{'class':'details-price js-order-value'}):
            rawPrice = soup.find(
                            'span',{'class':'details-price js-order-value'}).text.strip()
            if '.' in rawPrice:
                itemDic['currency'] = 'usd'
            else:
                itemDic['currency'] = 'eur'

        # Check if we can buy this item
        if soup.find('a', {'id':'addItemToCart'}):
                commercialDic = ast.literal_eval(
                                    soup.find('a', {'id':'addItemToCart'}).get('data-track-object'))

                if not commercialDic['ecommerce']['add']['products'][0]['price'] == '0.0':
                    itemDic['price'] = commercialDic['ecommerce']['add']['products'][0]['price']
                    itemDic['inStock'] = True

        # Check if we can get item colour
        if soup.find('span', {'class':'selected-swatch'}):
            itemDic['colour'] = soup.find('span', {'class':'selected-swatch'}).text

        elif soup.find('li', {'class':'selected'}):

            if soup.find('li', {'class':'selected'}).find('a'):
                if soup.find('li', {'class':'selected'}).find('a').get('data-swatch-name'):
                    itemDic['colour'] = soup.find(
                                            'li', {'class':'selected'}).find('a').get('data-swatch-name')

        # Small colour fix
        if itemDic['colour']:
            if 'Other ' in itemDic['colour']:
                itemDic['colour'] = itemDic['colour'].split(': ')[1]

        itemDic['processTime'] = round(time.time() - startTime, 3)

        pprint(itemDic)
        print(len(self.processedProducts), 'items')

        return itemDic
